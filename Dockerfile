# Définir l'image source
FROM ubuntu:20.04
# Ajouter un utilisateur 
RUN useradd -u 0829 insa
USER insa
# Définir le répertoire de travail temporaire pour la configuration du package
WORKDIR /usr/src/cache
ENV DEBIAN_FRONTEND noninteractive
# Update apt packages
# Exécuter des commandes dans votre conteneur
USER root
RUN apt update \
    && apt upgrade -y
## installe and upgrade pip3 version
RUN apt-get install python3-pip -y \
    && pip3 install --upgrade pip
## install pygame and numpy
RUN pip3 install pygame \
    && pip3 install numpy
USER insa
# Copier le répertoire 
COPY _opt /opt/carla
# Définir les volumes utilisables
VOLUME /opt/carla
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
